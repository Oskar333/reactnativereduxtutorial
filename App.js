import React, { Component } from 'react';
import { createMaterialTopTabNavigator, createAppContainer, createStackNavigator } from "react-navigation";
import Home from './src/components/Home';
import Screen from './src/components/Screen';

const AppNavigator = createMaterialTopTabNavigator(  
  {
    Home: Home,
    Screen: Screen   
  },
  {
    tabBarPosition: 'bottom',
    initialRouteName: "Home",
    tabBarOptions: {
      tabStyle: {
        backgroundColor: '#FFF',
      },
      style: {
        backgroundColor: 'FFF',
      },
      showLabel: true,
      activeTintColor: '#000',
      inactiveTintColor: '#cccccc'
    },
  }
);

const RootStack = createStackNavigator(
  {
    Main: {
      screen: AppNavigator        
    }   
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}