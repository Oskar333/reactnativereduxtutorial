import { INCREMENT_VALUE, DECREMENT_VALUE } from '../actions/types';

const initialState = {
    counter: 0
}
const valueReducer = (state = initialState, action) => {
    switch(action.type) {
        case INCREMENT_VALUE:
            return {
                counter: state.counter + 1    
            };
        case DECREMENT_VALUE:
            return {
                counter: state.counter - 1 
            }
        default:
            return state;
    }
}
export default valueReducer;