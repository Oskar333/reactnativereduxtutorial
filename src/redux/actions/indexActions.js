import { INCREMENT_VALUE, DECREMENT_VALUE } from './types';

export const incrementValue = () => {
    return {
        type: INCREMENT_VALUE,
        payload: null
    }
}

export const decrementValue = () => {
    return {
        type: DECREMENT_VALUE,
        payload: null
    }
}