import React, { Component } from 'react'
import { Text, View, Button, StyleSheet, TouchableOpacity } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import { connect } from 'react-redux';
import { incrementValue, decrementValue } from '../redux/actions/indexActions';

class Screen extends Component {
  constructor(props) {
    super(props);
  }

  _decrement = () => {
    this.props.removeValueFromCounter();
    console.log(this.props);
  }

  _increment = () => {
    this.props.addValueToCounter();
  }

  render() {
    return (
      <View style={styles.mainContainer}>       
        <NavigationEvents
          onDidFocus={payload => console.log('did focus',payload)}
        />
        <Text> Screen component </Text>
        <View style={styles.buttonContainer}> 
          <TouchableOpacity
            onPress={this._decrement}
            style={styles.button}
          >
            <Text style={styles.text}>-</Text>
          </TouchableOpacity>
            <Text style={styles.text}>{this.props.counter.counter}</Text>
          <TouchableOpacity
            onPress={this._increment}
            style={styles.button}
          >
            <Text style={styles.text}>+</Text>
          </TouchableOpacity>         
        </View>
          
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1, 
    alignItems: "center", 
    justifyContent: "center"
  },
  buttonContainer: {
    width: "100%",
    justifyContent: "space-around",
    flexDirection: "row",
    alignItems: "center"
  },
  button: {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:100,
    height:100,
    backgroundColor:'#FFF',
    borderRadius:100
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold'
  }
});

const mapStateToProps = state => {
  return {
    counter: state.counter
  }
}
const mapDispatchToProps = dispatch => {
  return {
    addValueToCounter: () => {
      dispatch(incrementValue())
    },
    removeValueFromCounter: () => {
      dispatch(decrementValue())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Screen)