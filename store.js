import { createStore, combineReducers, applyMiddleware } from 'redux';
import valueReducer from './src/redux/reducers/valueReducer';

const rootReducer = combineReducers({
    counter: valueReducer,
});

const configureStore = () => {
    return createStore(rootReducer, applyMiddleware(logger));
}

const logger = store => next => action => {
    console.log('dispatching', action)
    console.log('Current state', store.getState())
    let result = next(action)
    console.log('Next state', store.getState())
    return result
} 

export default configureStore;